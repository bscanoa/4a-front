import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/galeria',
    name: 'Galeria',
    // route level code-splitting
    // this generates a separate chunk (galeria.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "galeria" */ '../views/Galeria.vue')
  },
  {
    path: '/logIn',
    name: 'LogIn',
    // route level code-splitting
    // this generates a separate chunk (ingresar.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "ingresar" */ '../views/Ingresar.vue')
  },
  {
    path: '/registrar',
    name: 'Registrar',
    // route level code-splitting
    // this generates a separate chunk (registrar.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "registrar" */ '../views/Registrar.vue')
  },
  {
    path: '/cars',
    name: 'Cars',
    // route level code-splitting
    // this generates a separate chunk (registrar.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "registrar" */ '../views/Cars.vue')
  },
  
  {
    path: '/search',
    name: 'Search',
    // route level code-splitting
    // this generates a separate chunk (registrar.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "registrar" */ '../views/Search.vue')
  },
  {
    path: '/rent',
    name: 'Rent',
    // route level code-splitting
    // this generates a separate chunk (registrar.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "registrar" */ '../views/Reserve.vue'),
  },
  {
    path: '/cuenta',
    name: 'Account',
    // route level code-splitting
    // this generates a separate chunk (registrar.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "registrar" */ '../views/Account.vue'),
  },
  {
    path: '/reservar',
    name: 'Reserve',
    // route level code-splitting
    // this generates a separate chunk (registrar.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "registrar" */ '../views/Reserve.vue'),
  },
  {
    path: '/:category_slug',
    name: 'Category',
    // route level code-splitting
    // this generates a separate chunk (registrar.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "registrar" */ '../views/Category.vue')
  },
]


  

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
export default router
